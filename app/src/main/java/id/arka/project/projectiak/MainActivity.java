package id.arka.project.projectiak;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void newton(View view){
        Intent Newton = new Intent(MainActivity.this,Newton.class);
        MainActivity.this.startActivity(Newton);
    }

    public void archimedes(View view){
        Intent Archimedes = new Intent(MainActivity.this,Archimedes.class);
        MainActivity.this.startActivity(Archimedes);
    }

    public void lorentz(View view){
        Intent Lorentz = new Intent(MainActivity.this,Lorentz.class);
        MainActivity.this.startActivity(Lorentz);
    }
    public void pascal(View view){
        Intent Pascal = new Intent(MainActivity.this,Pascal.class);
        MainActivity.this.startActivity(Pascal);
    }
    public void einstein(View view){
        Intent Einstein = new Intent(MainActivity.this,Einstein.class);
        MainActivity.this.startActivity(Einstein);
    }
}
