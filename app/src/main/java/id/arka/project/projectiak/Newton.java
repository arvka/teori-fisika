package id.arka.project.projectiak;

import android.app.ActionBar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Newton extends AppCompatActivity {

    Double hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newton);
    }

    public void hitung(View v){
        EditText
                editG = (EditText) findViewById(R.id.textG),
                editR = (EditText) findViewById(R.id.textR),
                editM1 = (EditText) findViewById(R.id.textM1),
                editM2 = (EditText) findViewById(R.id.textM2);
        TextView txtF = (TextView) findViewById(R.id.textF);
        Integer
                g = Integer.parseInt(editG.getText().toString()),
                r = Integer.parseInt(editR.getText().toString()),
                m1 = Integer.parseInt(editM1.getText().toString()),
                m2 = Integer.parseInt(editM2.getText().toString());
        hasil = g * (m1 * m2) / Math.pow(r,2);
        txtF.setText("F = " + String.valueOf(hasil) + " N");
    }
}
