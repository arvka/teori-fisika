package id.arka.project.projectiak;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Archimedes extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_archimedes);
    }

    public void hitungWs(View v){
        EditText
                wt = (EditText) findViewById(R.id.textW),
                fat = (EditText) findViewById(R.id.textFa);
        Double
                w = Double.parseDouble(wt.getText().toString()),
                fa = Double.parseDouble(fat.getText().toString());
        Double hasil =  w - fa;
        TextView txthasil1 = (TextView) findViewById(R.id.textWs);
        txthasil1.setText("Ws = " + String.valueOf(hasil) + " Kg.m/s^2");
    }

    public void hitungFa(View v){
        EditText
                pcairt = (EditText) findViewById(R.id.textPcair),
                vbt = (EditText) findViewById(R.id.textVb),
                gt = (EditText) findViewById(R.id.textG);
        Double
                pcair = Double.parseDouble(pcairt.getText().toString()),
                vb = Double.parseDouble(vbt.getText().toString()),
                g = Double.parseDouble(gt.getText().toString());
        Double hasil =  pcair * vb * g;
        TextView txthasil2 = (TextView) findViewById(R.id.textFa2);
        txthasil2.setText("Ws = " + String.valueOf(hasil) + " Kg.m/s^2");
    }
}
